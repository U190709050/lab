public class GDCRec {
    public static void main(String[] args) {
        int number1 = 1071;
        int number2 = 462;
    
        int result =  gcd(1071, 462);
    
        System.out.println("GCD of" + number1 + " and " + number2  + " = " + result);
    
        }
    
        private static int gcd(int number1, int number2) {
    
            int remainder = number1 % number2;
            if(remainder == 0)
                return number2;
            return gcd(number2,remainder);    
            
        
        }
    
    }